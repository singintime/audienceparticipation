import random
import socket
import time
import sys
import thread
import threading

def send_msg():
  while 1:
    msg = sys.stdin.readline()
    s.send("hit")

BUFFER_SIZE = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((sys.argv[1], int(sys.argv[2])))
thread.start_new_thread(send_msg, ())
s.send("make:Test game")
interval = 1.0
while 1:
  data = s.recv(BUFFER_SIZE)
  if data == "":
    break;
  sys.stdout.write("received data: %s" % data)
  lines = data.split("\n")
  for line in lines:
    if line == "": continue
    tokens = line.split(":")
    if tokens[0] == "ownb" and random.random() > 0.1:
      msg = "hit:%s\n" % tokens[1]
      t = threading.Timer(interval, s.send, args=[msg])
      t.start()
      #interval -= 0.04
    elif tokens[0] == "serve":
      t = threading.Timer(4.0, s.send, args=["hit:r\n"])
      t.start()
    #elif tokens[0] == "good" or tokens[0] == "fail":
      #interval = 1.0
    elif tokens[0] == "win" or tokens[0] == "lose":
      #interval = 1.0
      s.send("make:Test game\n")
s.close()

