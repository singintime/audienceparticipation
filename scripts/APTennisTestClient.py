import socket
import time
import sys
import thread

def send_msg():
  while 1:
    msg = sys.stdin.readline()
    s.send(msg)

BUFFER_SIZE = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((sys.argv[1], int(sys.argv[2])))
thread.start_new_thread(send_msg, ())
while 1:
  data = s.recv(BUFFER_SIZE)
  if data == "":
    break;
  sys.stdout.write("received data: %s" % data)
s.close()

