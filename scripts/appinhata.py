import apcommon

class Game(apcommon.Game):
  def __init__(self, name, version, max_hits):
    apcommon.Game.__init__(self, name, version)
    self.reset(None, max_hits)
    self.rules.clear()
    self.rules["hit"] = self.hit
    self.rules["reset"] = self.reset

  def register(self, player):
    player.hits = 0
    apcommon.Game.register(self, player)

  def hit(self, player):
    if self.hits >= self.max_hits:
      player.send_line("miss")
      return
    player.hits += 1
    player.send_line("thud")
    self.hits += 1
    if self.hits >= self.max_hits:
      winner = None
      for p in self.players:
        p.send_line("crash:%d" % p.hits)
        if not winner or p.hits > winner.hits:
          winner = p
      winner.send_line("win")
      
  def reset(self, player, max_hits):
    try:
      self.max_hits = int(max_hits)
    except ValueError:
      player.send_line("nack:args")
      return
    self.hits = 0
    for p in self.players:
      p.hits = 0