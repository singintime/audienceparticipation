import unittest

class Server():
  def __init__(self):
    self.games = dict()
    
  def add_game(self, name, version):
    self.games[name] = version  
    
  def game_exists(self, name):
    return name in self.games
    
class APTests(unittest.TestCase):
  def testGameExistsTrue(self):
    s = Server()
    s.add_game("pippo", 1)
    self.failUnless(s.game_exists("pippo"))
  
  def testGameExistsTrue(self):
    s = Server()
    s.add_game("pippo", 1)
    self.failIf(s.game_exists("pippa"))
  
if __name__ == "__main__":
  unittest.main()