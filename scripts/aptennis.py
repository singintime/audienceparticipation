import apcommon
import datetime
import time
import threading

class Game(apcommon.Game):
  def __init__(self, name, version):
    apcommon.Game.__init__(self, name, version)
    self.rules["hit"] = self.hit
    
  def create_match(self, id, name, *args):
    try:
      tolerance = float(args[0])
    except IndexError, ValueError:
      tolerance = 0.5
    try:
      max_lives = int(args[1])
    except IndexError, ValueError:
      max_lives = 3
    return Team(self, id, name, tolerance, max_lives)
     
  def hit(self, player, direction):
    if not player.match:
      player.send_line("miss")
      return
    player.match.hit(player, direction)
     
class Team(apcommon.Match, threading.Thread):
  def __init__(self, game, id, name, tolerance, max_lives):
    threading.Thread.__init__(self)
    apcommon.Match.__init__(self, game, id, name)
    self.leader = None
    self.tolerance = tolerance
    self.max_lives = max_lives
    
  def is_joinable(self, player):
    return player not in self.players and player != self.leader
  
  def is_removable(self):
    return self.leader == None
    
  def join(self, player):
    if self.leader == None:
      self.leader = player
      player.send_line("lead:%s" % self.id)
      player.match = self
    else:
      player.hits = set()
      player.lives = self.max_lives
      apcommon.Match.join(self, player)

  def part(self, player):
    if player == self.leader:
      self.leader = None
      self.players.add(player)
    apcommon.Match.part(self, player)
      
  def hit(self, player, direction):
    if player == self.leader:
      self.leader_hit = (direction, datetime.datetime.now())
      t = threading.Timer(self.tolerance, self.judge)
      t.start()
    elif player in self.players:
      player.hits.add((direction, datetime.datetime.now()))
    else:
      player.send_line("miss")

  def judge(self):
    leader_hit_type, leader_hit_time = self.leader_hit
    for player in self.players.copy():
      in_sync = False
      for hit in player.hits.copy():
        hit_type, hit_time = hit
        delta = datetime.timedelta(seconds=self.tolerance)
        diff = leader_hit_time - hit_time
        if diff <= delta:
          if hit_type in leader_hit_type:
            in_sync = True
        else:
          player.hits.remove(hit)
      if in_sync:
        player.send_line("good:%d" % player.lives)
      else:
        player.lives -= 1
        player.send_line("fail:%d" % player.lives)
        if player.lives <= 0:
          self.part(player)
    
  