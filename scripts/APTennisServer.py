import datetime
import socket
import SocketServer
import sys
import threading
import time
import Tkinter

class Server(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
  daemon_threads = True
  allow_reuse_address = True
  
  def __init__(self, server_address, RequestHandlerClass):
        SocketServer.TCPServer.__init__(self, server_address, RequestHandlerClass)



class Player(SocketServer.BaseRequestHandler):
  def handle(self):
    self.id = "%s:%d" % self.client_address
    self.thuds = 0
    print("Client connected: %s" % self.id)
    PLAYERS.add(self);
    while True:
      try:
        data = self.request.recv(BUF_SIZE)
      except socket.error:
        break
      if not data:
        break
      data = data.replace(";","")
      sys.stdout.write(data)
      lines = data.split("\n")
      for line in lines:
        tokens = line.split(":")
        if   tokens[0] == "join": self.join_team(tokens[1])
        elif tokens[0] == "lead": self.lead_team(tokens[1])
        elif tokens[0] == "lhit": self.leader_hit(tokens[1], tokens[2])
        elif tokens[0] == "part": self.part_team()
        elif tokens[0] == "phit": self.player_hit(tokens[1])
        elif tokens[0] == "thud": self.crash_pinhata()
        elif tokens[0] == "PING": self.request.send("PONG")
        else: self.request.send("nack:Unrecognized message")
    self.logout()
    try:
      self.request.shutdown(socket.SHUT_RDWR)
      self.request.close()
    except socket.error:
      pass
    PLAYERS.discard(self)
    print("Closed connection with %s" % self.id)

  def check_role(self):
    for name, members in TEAMS.items():
      if self in members:
        return (name, False)
    for name, leader in LEADERS.items():
      if self is leader:
        return (name,True)
    return (None, None)

  def crash_pinhata(self):
    all_thuds = 0
    self.thuds += 1
    for player in PLAYERS:
      all_thuds += player.thuds
    if all_thuds >= MAX_THUDS:
      for player in PLAYERS:
        player.request.send("crash:%d" % player.thuds)
        player.thuds = 0;

  def join_team(self, team_name):
    self.logout()
    if team_name not in TEAMS:
      TEAMS[team_name] = set()
    team = TEAMS[team_name]
    self.lives = MAX_LIVES
    self.hits = set()
    team.add(self)
    self.request.send("join:%s" % team_name)
    print("%s joined team %s" % (self.id, team_name))

  def leader_hit(self, tolerance, hit_type):
    team_name, is_leader = self.check_role()
    if not team_name:
      self.request.send("nack:You must lead a team first")
      return
    if not is_leader:
      self.request.send("nack:You are a follower, you should send phit: messages!")
      return
    try:
      millis = int(tolerance)
    except ValueError:
      self.request.send("nack:Leader hit argument must be an integer")
      return
    leader_hit = (hit_type, datetime.datetime.now())
    judge = Judge(team_name, millis, leader_hit)
    judge.start()

  def lead_team(self, team_name):
    self.logout()
    LEADERS[team_name] = self
    self.request.send("lead:%s" % team_name)
    print("%s is leading team %s" % (self.id, team_name))

  def logout(self):
    team_name, is_leader = self.check_role()
    if team_name:
      if is_leader:
        del LEADERS[team_name]
      else:
        team = TEAMS[team_name]
        team.remove(self)
        if len(team) == 0:
          del TEAMS[team_name]
      return True
    return False

  def part_team(self):
    team_name, is_leader = self.check_role()
    if self.logout():
      self.request.send("part:%s" % team_name)
      print("%s left team %s" % (self.id, team_name))

  def player_hit(self, hit_type):
    team_name, is_leader = self.check_role()
    if not team_name:
      self.request.send("nack:You must register to a team first")
      return
    if is_leader:
      self.request.send("nack:You are a leader, you should send lhit: messages")
      return
    self.hits.add((hit_type, datetime.datetime.now()))

  def __repr__(self):
    return self.id



class Judge(threading.Thread):
  def __init__(self, team_name, tolerance, leader_hit):
    threading.Thread.__init__(self)
    self.team_name = team_name
    self.tolerance = tolerance
    self.leader_hit = leader_hit
    
  def run(self):
    if self.team_name not in TEAMS: return
    time.sleep(self.tolerance / 1000.0)
    leader_hit_type, leader_hit_time = self.leader_hit
    team = TEAMS[self.team_name].copy()
    for player in team:
      in_sync = False
      hits = player.hits.copy()
      for hit in hits:
        hit_type, hit_time = hit
        delta = datetime.timedelta(milliseconds=self.tolerance)
        diff = leader_hit_time - hit_time
        if diff <= delta:
          if leader_hit_type == hit_type:
            in_sync = True
        else:
          player.hits.remove(hit)
      if in_sync:
        player.request.send("good:%d" % player.lives)
      else:
        player.lives -= 1
        player.request.send("fail:%d" % player.lives)
        if player.lives <= 0:
          player.logout()

if __name__ == "__main__":
  PLAYERS = set()
  TEAMS = dict()
  LEADERS = dict()
  MAX_THUDS = 100
  MAX_LIVES = 3
  BUF_SIZE = 256
  
  server = Server((sys.argv[1], int(sys.argv[2])), Player)
  print("APTennis server started. Press Ctrl+C to quit.")
  try:
    server.serve_forever()
  except KeyboardInterrupt:
    sys.exit(0)