import apcommon
import random
import threading

class Game(apcommon.Game):
  def __init__(self, name, version):
    apcommon.Game.__init__(self, name, version)
    self.rules["ready"] = self.ready
    self.rules["hit"] = self.hit
    
  def create_match(self, id, name, *args):
    try:
      interval = float(args[0])
    except IndexError, ValueError:
      interval = 1.0
    try:
      max_lives = int(args[1])
    except IndexError, ValueError:
      max_lives = 3
    return Match(self, id, name, interval, max_lives)
    
  def ready(self, player):
    if player.match:
      player.match.ready(player)
    
  def hit(self, player, direction):
    if not player.match:
      player.send_line("miss")
      return
    player.match.hit(player, direction)
    
class Match(apcommon.Match):
  def __init__(self, game, id, name, interval, max_lives):
    apcommon.Match.__init__(self, game, id, name)
    self.interval = interval
    self.max_lives = max_lives
    self.active = None
    self.opponent = None
    self.has_ball = True
    self.direction = "c"
    self.thread = None
    
  def is_joinable(self, player):
    if player not in self.players and len(self.players) < 2:
      return True
    return False  
  
  def is_removable(self):
    return len(self.players) < 2
  
  def join(self, player):
    player.lives = self.max_lives
    apcommon.Match.join(self, player)
    if not self.active:
      self.active = player
    else:
      self.opponent = player
      self.started = True
      self.game.broadcast("del:%s" % self.id)
  
  def finish(self):
    if self.thread is not None:
      self.thread.cancel()
    apcommon.Match.finish(self)

  def ready(self, player):
    player.send_line("lives:%s" % self.max_lives)
    if self.active and self.opponent:
      self.active.send_line("serve")
      self.opponent.send_line("recv")
  
  def swap_players(self):
    swap = self.active
    self.active = self.opponent
    self.opponent = swap
    
  def hit(self, player, direction):
    if len(self.players) != 2 or player != self.active or not self.has_ball or self.direction not in direction:
      player.send_line("miss")
      return
    if self.thread:
      self.thread.cancel()
    self.direction = "lr"[random.randint(0,1)]
    self.active.send_line("ownh")
    self.opponent.send_line("%s:%s" % ("opph", self.direction))
    self.has_ball = False
    self.thread = threading.Timer(self.interval, self.bounce)
    self.thread.start()

  def bounce(self):
    self.active.send_line("oppb")
    self.opponent.send_line("%s:%s" % ("ownb", self.direction))
    self.thread = threading.Timer(0.2 * self.interval, self.arrived)
    self.thread.start()

  def arrived(self):
    self.swap_players()
    self.has_ball = True
    self.thread = threading.Timer(1.6 * self.interval, self.score)
    self.thread.start()

  def score(self):
    self.active.lives -= 1
    if self.active.lives > 0:
      self.swap_players()
      self.has_ball = True
      self.direction = "c"
      self.active.send_line("good:%s:%s" % (self.active.lives, self.opponent.lives))
      self.opponent.send_line("fail:%s:%s" % (self.opponent.lives, self.active.lives)) 
      self.active.send_line("serve")
      self.opponent.send_line("recv")
    else:
      self.active.send_line("lose")
      self.opponent.send_line("win")
      self.game.remove_match(self.opponent, self.id)
