import apcommon
import aphall
import appinhata
import aptennis
import melodybounce
import sonictennis

if __name__ == "__main__":
  apPinhata = appinhata.Game("APPinhata", "1", 10)
  apTennis = aptennis.Game("APTennis", "1")
  sonicTennis = sonictennis.Game("SonicTennis", "4")
  melodyBounce = melodybounce.Game("MelodyBounce", "1")
  apHall = aphall.Game("APHall", "1", [apPinhata, apTennis])
  games = [apHall, apPinhata, apTennis, sonicTennis, melodyBounce]
  apcommon.apserver_start(games)