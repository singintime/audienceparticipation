import apcommon

class Game(apcommon.Game):
  def __init__(self, name, version, games):
    apcommon.Game.__init__(self, name, version)
    self.games = games
    self.curr_game = None
    self.curr_version = None
    self.rules.clear()
    self.rules["setgame"] = self.set_game
    self.rules["getgame"] = self.get_game
     
  def set_game(self, player, name, version):
    self.curr_game = name
    self.curr_version = version
    self.broadcast("change:%s:%s" % (name, version))
    for game in self.games:
      game.broadcast("change:%s:%s" % (name, version))
      
  def get_game(self, player):
    if self.curr_game and self.curr_version:
      player.send_line("change:%s:%s" % (self.curr_game, self.curr_version))