import cv2
import numpy
import socket

class Threshold:
  def __init__(self, hValue, hTol, sValue, sTol, vValue, vTol):
    self.hValue = hValue
    self.hTol = hTol
    self.sValue = sValue
    self.sTol = sTol
    self.vValue = vValue
    self.vTol = vTol

  @property
  def hMin(self):
    return (self.hValue - self.hTol) % 180

  @property
  def hMax(self):
    return (self.hValue + self.hTol) % 180

  @property
  def sMin(self):
    return max(self.sValue - self.sTol, 0)

  @property
  def sMax(self):
    return min(self.sValue + self.sTol, 255)

  @property
  def vMin(self):
    return max(self.vValue - self.vTol, 0)

  @property
  def vMax(self):
    return min(self.vValue + self.vTol, 255)

class ColorTracker:
  def __init__(self, firstFrame):
    self.buf = cv2.cvtColor(firstFrame, cv2.COLOR_BGR2HSV)
    self.thresholds = dict()

  def track(self, img):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    blur = cv2.GaussianBlur(hsv, (3, 3), 0)
    self.buf = cv2.addWeighted(blur, 0.75, self.buf, 0.25, 0)
    h = self.buf[:,:,[0]]
    s = self.buf[:,:,[1]]
    v = self.buf[:,:,[2]]
    objects = dict()
    for label, thresh in self.thresholds.items():
      if thresh.hMin < thresh.hMax:
        mask = cv2.inRange(h, int(thresh.hMin), int(thresh.hMax))
      else:
        mask = cv2.inRange(h, int(thresh.hMax), int(thresh.hMin))
        mask = 179 - mask
      mask &= cv2.inRange(s, int(thresh.sMin), int(thresh.sMax))
      mask &= cv2.inRange(v, int(thresh.vMin), int(thresh.vMax))
      self.mask = mask
      mms = cv2.moments(mask)
      if (mms["m00"] > 0):
        obj = dict()
        obj["size"] = int(mms["m00"] / 255)
        obj["pos"] = (int(mms["m10"]/mms["m00"]), int(mms["m01"]/mms["m00"]))
        objects[label] = obj
    return objects

  def onmouse(self, event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
      pix = self.buf[y,x]
      print(pix)
      self.thresholds["obj%d" % len(self.thresholds)] = Threshold(pix[0], 16, pix[1], 32, pix[2], 32)

if __name__ == "__main__":
  cv2.namedWindow("w1")
  cap = cv2.VideoCapture(0)
  status, frame = cap.read()
  tracker = ColorTracker(frame)
  cv2.setMouseCallback("w1",tracker.onmouse)
  #tracker.addThreshold("obj1", 170, 10, 128, 255, 128, 255)
  sock = socket.socket()
  try:
    sock.connect(("localhost", 3000))
  except socket.error:
    pass

  while True:
    status, frame = cap.read()
    objects = tracker.track(frame)
    for obj in objects.values():
      if obj["size"] > 500:
        cv2.rectangle(frame, obj["pos"], (obj["pos"][0] + 5, obj["pos"][1] + 5), (0,255,0), thickness=-1)
    cv2.imshow("w1",frame)
    #print(objects)
    cv2.waitKey(10)
