import apcommon
import random
import threading

class Game(apcommon.Game):
  def __init__(self, name, version):
    apcommon.Game.__init__(self, name, version)
    self.rules["shake"] = self.shake
    
  def create_match(self, id, name, *args):
    try:
      interval = float(args[0])
    except IndexError, ValueError:
      interval = 1.0
    return Match(self, id, name, interval)
    
  def shake(self, player):
    if not player.match:
      player.send_line("miss")
      return
    player.match.shake(player)
    
class Match(apcommon.Match):
  def __init__(self, game, id, name, interval):
    apcommon.Match.__init__(self, game, id, name)
    self.interval = interval
    self.active = None
    self.pos = 0
    self.has_ball = True
    self.thread = None
    self.tune = [("C",1),("C",1),("G",1),("G",1),("A",1),("A",1),("G",2),
                 ("F",1),("F",1),("E",1),("E",1),("D",1),("D",1),("C",2)]
  
  def pick_player(self):
    self.active = random.sample(self.players, 1)[0]
  
  def restart(self):
    self.pos = 0
    if self.thread:
      self.thread.cancel()
    self.thread = threading.Timer(4 * self.interval, self.expire)
    self.thread.start()
                 
  def shake(self, player):
    if self.active == None:
      self.pick_player()
      self.restart()
    elif player != self.active or not self.has_ball:
      player.send_line("fail")
      self.restart()
      return
    self.has_ball = False
    self.pick_player()

  def expire(self):
    if self.has_ball and self.pos != 0:
      self.active.send_line("fail")
      self.restart()
    elif self.pos == len(self.tune):
      for player in self.players:
        player.send_line("win")
      self.game.remove_match(self.active, self.id)
    else:
      self.has_ball = True
      pitch = self.tune[self.pos][0]
      time = self.tune[self.pos][1]
      self.active.send_line("note:%s" % pitch)
      self.thread = threading.Timer(time * self.interval, self.expire)
      self.thread.start()
      self.pos += 1
