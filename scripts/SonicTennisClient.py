import random
import socket
import time
import sys
import threading

class BotGame(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self)
    self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.has_started = False

  def connect(self):
    try:
      self.socket.connect((sys.argv[1], int(sys.argv[2])))
    except (socket.error):
      time.sleep(5)
      self.connect()

  def run(self):
    self.connect()
    s = self.socket
    s.send("play:SonicTennis:4\n")
    match_name = sys.argv[3]
    interval = float(sys.argv[4])
    fail_rate = float(sys.argv[5])
    lives = 3
    t = None
    while 1:
      data = s.recv(BUFFER_SIZE)
      if data == "":
        break;
      sys.stdout.write("received data: %s" % data)
      lines = data.split("\n")
      for line in lines:
        if line == "": continue
        tokens = line.split(":")
        if tokens[0] == "play":
          s.send("make:%s:%f:%d" % (match_name, interval, lives))
        elif tokens[0] == "join":
          s.send("ready\n")
        elif tokens[0] == "ownb" and random.random() > fail_rate:
          msg = "hit:%s\n" % tokens[1]
          t = threading.Timer(interval, s.send, args=[msg])
          t.start()
          #interval -= 0.04
        elif tokens[0] == "serve":
          if not self.has_started:
            newbot = BotGame()
            newbot.start()
            self.has_started = True
          t = threading.Timer(4.0, s.send, args=["hit:c\n"])
          t.start()
        #elif tokens[0] == "good" or tokens[0] == "fail":
          #interval = 1.0
        elif tokens[0] == "part":
          #interval = 1.0
          if t is not None:
            t.cancel()
          s.close()
          return;

BUFFER_SIZE = 1024

bot = BotGame()
bot.start()
