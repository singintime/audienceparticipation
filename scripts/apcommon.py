import SocketServer
import datetime
import inspect
import socket
import sys

BUF_SIZE = 256

class Server(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
  daemon_threads = True
  allow_reuse_address = True
  
  def __init__(self, server_address, RequestHandlerClass, games):
    SocketServer.TCPServer.__init__(self, server_address, RequestHandlerClass)
    self.games = games

  def register(self, player, game_name, game_version):
    self.unregister(player)
    games = [game for game in self.games if game.name == game_name]
    if games == []:
      player.send_line("nack:game")
      return
    games = [game for game in games if game.version == game_version]
    if games == []:
      player.send_line("nack:version")
      return
    game.register(player)

  def unregister(self, player):
    player.part()
    player.leave()



class Game():
  def __init__(self, name, version):
    self.name = name
    self.version = version
    self.players = set()
    self.matches = dict()
    self.rules = {"make"  : self.make_match,
                  "part"  : self.part_match,
                  "join"  : self.join_match,
                  "list"  : self.list_matches,
                  "leave" : self.leave}

  def execute(self, player, command, *args):
    if command not in self.rules:
      player.send_line("nack:command")
      return None
    rule = self.rules[command]
    arg_info = inspect.getargspec(rule)
    argc = len(arg_info.args) - 2
    has_varargs = arg_info.varargs != None
    if (not has_varargs and len(args) != argc) or (has_varargs and len(args) < argc):
      player.send_line("nack:args")
      return None
    return rule(player, *args)
    
  def broadcast(self, message):
    for player in self.players.copy():
      player.send_line(message)

  def register(self, player):
    self.players.add(player)
    player.game = self
    player.send_line("play:%s:%s" % (self.name, self.version))

  def leave(self, player):
    if player in self.players:
      self.players.remove(player)
      player.game = None
      player.send_line("leave:%s:%s" % (self.name, self.version))
  
  def create_match(self, id, name, *args):
    return Match(self, id, name)

  def remove_match(self, player, match_id):
    if match_id in self.matches:
      match = self.matches[match_id]
      match.finish()
      del self.matches[match.id]
      self.broadcast("del:%s" % match_id)

  def make_match(self, player, match_name, *args):
    match_count = 0
    while True:
      match_id = "%s_%d" % (match_name, match_count)
      if match_id not in self.matches:
        match = self.create_match(match_id, match_name, *args)
        self.matches[match_id] = match
        self.join_match(player, match_id)
        self.broadcast("new:%s" % match_id)
        break
      match_count += 1

  def part_match(self, player):
    player.part()

  def join_match(self, player, match_id):
    self.part_match(player)
    if match_id not in self.matches:
      player.send_line("del:%s" % match_id)
      return
    match = self.matches[match_id]
    match.join(player)

  def list_matches(self, player):
    for match_id, match in self.matches.items():
      if match.is_joinable(player):
        player.send_line("new:%s" % match_id)



class Match():
  def __init__(self, game, id, name):
    self.id = id
    self.name = name
    self.game = game
    self.players = set()

  def is_joinable(self, player):
    return player not in self.players

  def is_removable(self):
    return len(self.players) == 0

  def remove_player(self, player):
    if player in self.players:
      self.players.remove(player)
      player.match = None
      player.send_line("part:%s" % self.id)

  def join(self, player):
    if not self.is_joinable(player):
      player.send_line("del:%s" % self.id)
      return
    self.players.add(player)
    player.match = self
    player.send_line("join:%s" % self.id)

  def part(self, player):
    self.remove_player(player)
    if self.is_removable():
      self.game.remove_match(player, self.id)

  def finish(self):
    for player in self.players.copy():
      self.remove_player(player)



class Player(SocketServer.BaseRequestHandler):
  def setup(self):
    self.game = None
    self.match = None

  def handle(self):
    print("Client connected: %s:%d" % self.client_address)
    while True:
      try:
        data = self.request.recv(BUF_SIZE)
      except socket.error:
        break
      if not data:
        break 
      data = data.replace(";\n", "\n")
      lines = data.split("\n")
      for line in lines:
        if line == "":
          break
# LOGGING
        self.log(self.client_address, self.server.server_address, line);
        tokens = line.split(":")
        if tokens[0] == "play":
          if len(tokens[1:]) < 2:
            self.send_line("nack:args")
            break
          self.server.register(self, tokens[1], tokens[2])
        elif not self.game:
          self.send_line("nack:register")
          break
        else:
          self.game.execute(self, tokens[0], *tokens[1:])
    try:
      self.request.shutdown(socket.SHUT_RDWR)
      self.request.close()
    except socket.error:
      pass
    print("Closed connection with %s:%d" % self.client_address)

  def finish(self):
    self.server.unregister(self)
  
  def part(self):
    if self.match:
      self.match.part(self)

  def leave(self):
    if self.game:
      self.game.leave(self)
    
  def send_line(self, msg):
    try:
      self.request.send("%s\n" % msg)
# LOGGING
      self.log(self.server.server_address, self.client_address, msg);
    except socket.error:
      pass

  def log(self, sender, receiver, msg):
    logfile = open("ap_log.txt", "a")
    logfile.write("%s;%s;%s;%s\n" % (sender, receiver, datetime.datetime.now(), msg))
    logfile.close()
    
def apserver_start(games):
  server = Server((sys.argv[1], int(sys.argv[2])), Player, games)
  print("Audience Participation server started. Press Ctrl+C to quit.")
  try:
    server.serve_forever()
  except KeyboardInterrupt:
    sys.exit(0)
