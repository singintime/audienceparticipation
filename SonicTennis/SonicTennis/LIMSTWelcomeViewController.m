//
//  LIMSTWelcomeViewController.m
//  SonicTennis
//
//  Created by AAU-CPH0065 on 01/03/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import "LIMSTWelcomeViewController.h"
#import "LIMSTHowtoViewController.h"
#import "LIMSTAppDelegate.h"

@interface LIMSTWelcomeViewController () {
    @private
    LIMSTAppDelegate *app;
}
@end

@implementation LIMSTWelcomeViewController
@synthesize buttonContainer, howtoButton, playButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        app = (LIMSTAppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[buttonContainer layer] setMasksToBounds:YES];
    [[buttonContainer layer] setCornerRadius:10];
    [[playButton layer] setMasksToBounds:YES];
    [[playButton layer] setCornerRadius:10];
    [[howtoButton layer] setMasksToBounds:YES];
    [[howtoButton layer] setCornerRadius:10];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[app sound] loop:@"tennis_match"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)play:(id)sender {
    [app sendPlay];
}

- (IBAction)howto:(id)sender {
    [app crossFadeView:app.howtoController.view];
}

@end
