//
//  LIMSTAppDelegate.m
//  SonicTennis
//
//  Created by AAU-CPH0065 on 10/01/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import "LIMSTAppDelegate.h"
#import "LIMSTHowtoViewController.h"
#import "LIMSTMatchViewController.h"
#import "LIMSTPlayViewController.h"
#import "LIMSTRacketViewController.h"
#import "LIMSTWelcomeViewController.h"

@interface LIMSTAppDelegate () {
    @private
    CGFloat moveT, stopT;
    NSString *address, *protocolName;
    NSUInteger *port, protocolVersion;
    BOOL isRegistered;
    CGRect screenCoords;
    UIView *topView;
}
@end

@implementation LIMSTAppDelegate
@synthesize howtoController, matchController, playController, racketController, welcomeController, network, sound;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    // APPLICATION SETUP
    protocolName = @"SonicTennis";
    protocolVersion = 4;
    address = @"centos6.lim.di.unimi.it";
    port = 27986;
    
    // getting screen size
    UIScreen *screen = [UIScreen mainScreen];
    CGSize size = screen.bounds.size;
    screenCoords = CGRectMake(0, 0, size.width, size.height);
    
    // initializing network messaging
    isRegistered = NO;
    network = [[APNetworkManager alloc] init];
    [network addObserver:self];
    
    
    // initializing sound engine
    sound = [[APSoundManager alloc] init];
    [sound addSamples:[NSArray arrayWithObjects:@"service", @"tennis_ambient", @"hit_close", @"hit_far", @"bounce_close", @"bounce_far",
                                                @"swosh", @"good", @"fail", @"you_win", @"you_lose", @"tennis_match", nil] ofType:@"wav"];
    
    // instantiating view controllers
    howtoController = [[LIMSTHowtoViewController alloc] initWithNibName:@"LIMSTHowtoViewController" bundle:nil];
    matchController = [[LIMSTMatchViewController alloc] initWithNibName:@"LIMSTMatchViewController" bundle:nil];
    playController = [[LIMSTPlayViewController alloc] initWithNibName:@"LIMSTPlayViewController" bundle:nil];
    racketController = [[LIMSTRacketViewController alloc] initWithNibName:@"LIMSTRacketViewController" bundle:nil];
    welcomeController = [[LIMSTWelcomeViewController alloc] initWithNibName:@"LIMSTWelcomeViewController" bundle:nil];
    self.window.rootViewController = self.racketController;
    [self.window makeKeyAndVisible];
    
    // attempting connection
    [network connectWithAddress:address port:port];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)fadeInView:(UIView *)view {
    if (view) {
        topView = view;
        [view setFrame:screenCoords];
        [view setAlpha:0];
        [self.window addSubview:view];
        [self.window bringSubviewToFront:view];
        [UIView animateWithDuration:0.5 animations:^{[view setAlpha:1.0];}];
    }
}

- (void)fadeOutView:(UIView *)view {
    if (view) {
        [UIView animateWithDuration:0.5 animations:^{[view setAlpha:0.0];} completion:^(BOOL b){[view removeFromSuperview];}];
    }
}

- (void)crossFadeView:(UIView *)view {
    if (view != topView) {
        [self fadeOutView:topView];
        [self fadeInView:view];
    }
}

- (void)sendPlay {
    NSString *playMessage = [NSString stringWithFormat:@"play:%@:%d", protocolName, protocolVersion];
    [[self network] send:playMessage];
}

- (void)handleMessage:(NSString *)message {
    NSLog(@"%@", message);
    NSArray *tokens = [message componentsSeparatedByString: @":"];
    NSString *action = tokens[0];
    if ([action isEqualToString:@"play"] || [action isEqualToString:@"part"]) {
        [self crossFadeView:self.matchController.view];
    }
    else if ([action isEqualToString:@"join"]) {
        [self crossFadeView:self.playController.view];
    }
    else if ([action isEqualToString:@"leave"]) {
        [self crossFadeView:self.welcomeController.view];
    }
    else if ([action isEqualToString:@"nack"] && [tokens[1] isEqualToString:@"version"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upgrade you App!"
                                                        message:@"Your Sonic Tennis version is outdated... Please get the new one!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)handleStatus:(NSString *)status onStream:(NSString *)ioStream {
    NSLog(@"%@", status);
    if ([status isEqualToString:@"space"] && [ioStream isEqualToString:@"out"]) {
        if (!isRegistered) {
            [self crossFadeView:self.welcomeController.view];
            isRegistered = YES;
        }
    }
    else if ([status isEqualToString:@"closed"] || [status isEqualToString:@"error"] || [status isEqualToString:@"sendfailed"]) {
        isRegistered = NO;
        if (!self.connectionAlert) {
            self.connectionAlert = [[UIAlertView alloc] initWithTitle:@"Connection error!"
                                                            message:@"Please check you internet settings and try to reconnect"
                                                           delegate:self
                                                  cancelButtonTitle:@"Reconnect"
                                                  otherButtonTitles:nil];
            [self.connectionAlert show];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == self.connectionAlert) {
        [network connectWithAddress:address port:port];
        self.connectionAlert = nil;
    }
}

@end
