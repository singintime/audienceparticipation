//
//  LIMSTMatchesViewController.m
//  SonicTennis
//
//  Created by AAU-CPH0065 on 28/02/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import "LIMSTMatchViewController.h"
#import "LIMSTAppDelegate.h"

@interface LIMSTMatchViewController () {
@private
    NSString *selectedMatch;
    LIMSTAppDelegate *app;
}
@end

@implementation LIMSTMatchViewController
@synthesize matches, matchName, matchSelector, makeButton, joinButton, backButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        matches = [[NSMutableArray alloc] init];
        app = (LIMSTAppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [matchName setBackgroundColor:[UIColor colorWithRed:0.125 green:0.125 blue:0.125 alpha:1]];
    [[matchSelector layer] setCornerRadius:10];
    [[makeButton layer] setMasksToBounds:YES];
    [[makeButton layer] setCornerRadius:10];
    [[joinButton layer] setMasksToBounds:YES];
    [[joinButton layer] setCornerRadius:10];
    [[backButton layer] setMasksToBounds:YES];
    [[backButton layer] setCornerRadius:10];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[app network] addObserver:self];
    [matches removeAllObjects];
    [matchSelector reloadData];
    [[app network] send:@"list"];
    [[app sound] loop:@"tennis_match"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[app network] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [matches count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Identifier for retrieving reusable cells.
    static NSString *cellIdentifier = @"MatchesCellIdentifier";
    
    // Attempt to request the reusable cell.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // No cell available - create one.
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    
    // Set the text of the cell to the row index.
    NSString *match_id = [matches objectAtIndex:[indexPath row]];
    NSArray *tokens = [match_id componentsSeparatedByString: @"_"];
    cell.textLabel.text = tokens[0];
    cell.detailTextLabel.text = match_id;
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedMatch = [matches objectAtIndex:[indexPath row]];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath row] % 2) {
        cell.backgroundColor = [UIColor colorWithRed:0.15 green:0.15 blue:0.15 alpha:1];
    } else {
        cell.backgroundColor = [UIColor colorWithRed:0.125 green:0.125 blue:0.125 alpha:1];
    }
    cell.textLabel.textColor = [UIColor colorWithWhite:230.0f/255.0f alpha:1];
}

- (void)handleMessage:(NSString *)message {
    NSArray *tokens = [message componentsSeparatedByString: @":"];
    NSString *action = tokens[0];
    if ([action isEqualToString:@"new"]) {
        [matches removeObject:tokens[1]];
        [matches addObject:tokens[1]];
        [matchSelector reloadData];
    }
    else if ([action isEqualToString:@"del"]) {
        [matches removeObject:tokens[1]];
        [matchSelector reloadData];
    }
}

- (void)handleStatus:(NSString *)status onStream:(NSString *)ioStream {
    
}

- (IBAction)newMatch:(id)sender {
    if (![matchName.text isEqualToString:@""]) {
        [[app network] send:[NSString stringWithFormat:@"make:%@", matchName.text]];
    }
}

- (IBAction)joinMatch:(id)sender {
    if (selectedMatch) {
        [[app network] send:[NSString stringWithFormat:@"join:%@", selectedMatch]];
    }
}

- (IBAction)back:(id)sender {
    [[app network] send:@"leave"];
}
@end
