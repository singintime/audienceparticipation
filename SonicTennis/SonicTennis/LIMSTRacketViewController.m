//
//  LIMSTRacketViewController.m
//  SonicTennis
//
//  Created by AAU-CPH0065 on 04/03/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import "LIMSTRacketViewController.h"

@interface LIMSTRacketViewController ()

@end

@implementation LIMSTRacketViewController
@synthesize racketImg;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIScreen *screen = [UIScreen mainScreen];
    CGSize size = screen.bounds.size;
    CGFloat scale = screen.scale;
    if (size.width == 320.0f && size.height == 568.0f && scale == 2.0f) {
        [racketImg setImage:[UIImage imageNamed:@"racket-568h.png"]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
