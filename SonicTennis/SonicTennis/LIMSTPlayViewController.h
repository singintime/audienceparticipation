//
//  LIMSTPlayViewController.h
//  SonicTennis
//
//  Created by AAU-CPH0065 on 14/01/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LIMSTPlayViewController : UIViewController <APMotionDelegate, APNetworkDelegate>

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *leaveButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *youScore;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *oppScore;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *waitView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *waitRect;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *waitImg;

- (IBAction)leaveMatch:(id)sender;

@end
