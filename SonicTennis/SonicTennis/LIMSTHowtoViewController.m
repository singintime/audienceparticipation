//
//  LIMSTHowtoViewController.m
//  SonicTennis
//
//  Created by AAU-CPH0065 on 01/03/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import "LIMSTHowtoViewController.h"
#import "LIMSTMatchViewController.h"
#import "LIMSTAppDelegate.h"

@interface LIMSTHowtoViewController () {
    @private
    LIMSTAppDelegate *app;
    int status;
    NSTimer *tapTimer, *soundTimer, *loopTimer;
    BOOL canSkip;
}
@end

@implementation LIMSTHowtoViewController
@synthesize howtoLabel, tapLabel, howtoImage, leftImage, rightImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        app = (LIMSTAppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self hideAll];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    status = 0;
    canSkip = YES;
    [self hideAll];
    [self proceed:nil];
    [[app sound] stop:@"tennis_match"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)hideAll {
    howtoLabel.alpha = 0;
    tapLabel.alpha = 0;
    howtoImage.alpha = 0;
    leftImage.alpha = 0;
    rightImage.alpha = 0;
}

- (void)showTap {
    [UIView animateWithDuration:0.5 animations:^{tapLabel.alpha = 1;}];
    canSkip = YES;
}

- (void)playSequence:(NSTimer *)timer {
    NSArray *sequence = (NSArray *)[timer userInfo];
    if ([sequence count] == 0) return;
    NSString *sample = [sequence[0] objectForKey:@"sample"];
    CGFloat interval = [[sequence[0] objectForKey:@"interval"] floatValue];
    CGFloat panValue = [[sequence[0] objectForKey:@"panValue"] floatValue];
    [[app sound] pan:sample withValue:panValue];
    [[app sound] play:sample];
    NSRange range;
    range.location = 1;
    range.length = [sequence count] - 1;
    NSArray *newSequence = [sequence subarrayWithRange:range];
    soundTimer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(playSequence:) userInfo:newSequence repeats:NO];
}

- (NSDictionary *)soundToSequence:(NSString *)sample withInterval:(CGFloat)interval andPan:(CGFloat)panValue {
    NSArray *objects = [NSArray arrayWithObjects:sample, [NSNumber numberWithFloat:interval], [NSNumber numberWithFloat:panValue], nil];
    NSArray *keys = [NSArray arrayWithObjects:@"sample", @"interval", @"panValue", nil];
    NSDictionary *result = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    return result;
}

- (IBAction)proceed:(id)sender {
    if (!canSkip) return;
    canSkip = NO;
    if (tapTimer) {
        [tapTimer invalidate];
        tapTimer = nil;
    }
    if (soundTimer) {
        [soundTimer invalidate];
        soundTimer = nil;
    }
    if (loopTimer) {
        [loopTimer invalidate];
        loopTimer = nil;
    }
    NSString *instructionText, *imageName;
    NSArray *sequence;
    CGFloat leftAlpha = 0;
    CGFloat rightAlpha = 0;
    switch (status) {
        case 0:
            instructionText = @"Hold your device with your dominant hand and wear your headphones. When you hear the bouncing ball, swing your arm from above your head to serve.";
            imageName = @"howto_serve.png";
            leftAlpha = 0;
            rightAlpha = 0;
            sequence = [[NSArray alloc] init];
            [[app sound] loop:@"service"];
            break;
        case 1:
            instructionText = @"When you hear the ball bouncing on your side of the court, it is your turn to reply. If the sound comes from your dominant side, you must perform a forehand.";
            imageName = @"howto_forehand.png";
            leftAlpha = 0.5;
            rightAlpha = 0;
            sequence = [NSArray arrayWithObjects:[self soundToSequence:@"hit_close" withInterval:1.0 andPan:0.0],
                                                 [self soundToSequence:@"bounce_far" withInterval:1.0 andPan:0.0],
                                                 [self soundToSequence:@"hit_far" withInterval:1.0 andPan:-0.9],
                                                 [self soundToSequence:@"bounce_close" withInterval:1.0 andPan:-0.9],
                                                 nil];
            [[app sound] stop:@"service"];
            break;
        case 2:
            instructionText = @"If the sound comes from your non-dominant side, you must perform a backhand instead.";
            imageName = @"howto_backhand.png";
            leftAlpha = 0;
            rightAlpha = 0.5;
            sequence = [NSArray arrayWithObjects:[self soundToSequence:@"hit_close" withInterval:1.0 andPan:0.0],
                                                 [self soundToSequence:@"bounce_far" withInterval:1.0 andPan:0.0],
                                                 [self soundToSequence:@"hit_far" withInterval:1.0 andPan:0.9],
                                                 [self soundToSequence:@"bounce_close" withInterval:1.0 andPan:0.9],
                                                 nil];
            break;
        case 3:
             instructionText = @"Keep a constant rhythm to keep the ball into play. The first player who misses three balls loses the game. Now get on the court and challenge your opponents!";
            imageName = @"howto_serve.png";
            leftAlpha = 0;
            rightAlpha = 0;
            sequence = [NSArray arrayWithObjects:[self soundToSequence:@"hit_close" withInterval:1.0 andPan:0.0],
                        [self soundToSequence:@"bounce_far" withInterval:1.0 andPan:0.0],
                        [self soundToSequence:@"hit_far" withInterval:1.0 andPan:0.0],
                        [self soundToSequence:@"bounce_close" withInterval:1.0 andPan:0.0],
                        nil];
            loopTimer = [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(playSequence:) userInfo:sequence repeats:YES];
            break;
        default:
            [[app sound] stop:@"service"];
            [app sendPlay];
            return;
    }
    [UIView animateWithDuration:0.5 animations:^{[self hideAll];} completion:^(BOOL b){
        howtoLabel.text = instructionText;
        howtoImage.image = [UIImage imageNamed:imageName];
        [UIView animateWithDuration:0.5 animations:^{
            howtoLabel.alpha = 1;
            howtoImage.alpha = 1;
            leftImage.alpha = leftAlpha;
            rightImage.alpha = rightAlpha;
        }];
    }];
    tapTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(showTap) userInfo:nil repeats:NO];
    soundTimer = [NSTimer scheduledTimerWithTimeInterval:0.0 target:self selector:@selector(playSequence:) userInfo:sequence repeats:NO];
    status++;
}
@end
