//
//  LIMSTPlayViewController.m
//  SonicTennis
//
//  Created by AAU-CPH0065 on 14/01/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import "LIMSTPlayViewController.h"
#import "LIMSTAppDelegate.h"

@interface LIMSTPlayViewController () {
    @private
    LIMSTAppDelegate *app;
    APMotionManager *motion;
    NSString *motionMessage;
}
@end

@implementation LIMSTPlayViewController
@synthesize leaveButton, youScore, oppScore, waitView, waitRect, waitImg;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        app = (LIMSTAppDelegate *)[[UIApplication sharedApplication] delegate];
        motionMessage = nil;
        motion = [[APMotionManager alloc] initWithUpdateInterval:0.04];
        [motion addObserver:self];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[leaveButton layer] setMasksToBounds:YES];
    [[leaveButton layer] setCornerRadius:10];
    [[waitRect layer] setMasksToBounds:YES];
    [[waitRect layer] setCornerRadius:10];
    NSArray * imageArray  = [[NSArray alloc] initWithObjects:
                             [UIImage imageNamed:@"wait_0.png"],
                             [UIImage imageNamed:@"wait_1.png"],
                             [UIImage imageNamed:@"wait_2.png"],
                             [UIImage imageNamed:@"wait_3.png"],
                             nil];
	waitImg.animationImages = imageArray;
	waitImg.animationDuration = 1.0;
	waitImg.contentMode = UIViewContentModeBottomLeft;
	[waitImg startAnimating];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [motion start];
    [[app network] addObserver:self];
    [[app network] send:@"ready"];
    [[app sound] stop:@"tennis_match"];
    [[app sound] loop:@"tennis_ambient"];
    [waitView setAlpha:1.0];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [motion stop];
    [[app network] removeObserver:self];
    [[app sound] stop:@"service"];
    [[app sound] stop:@"tennis_ambient"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleMotionWithAcceleration:(CMAcceleration)acc rotation:(CMRotationRate)rot attitude:(CMAttitude *)att {
    if (!motionMessage) {
        BOOL isUpright = att.pitch > 0.25;
        BOOL isOnLeftSide = att.roll < 0;
        BOOL isOnRightSide = att.roll >= 0;
        BOOL movesScreenFirst = acc.z < -1.0;
        BOOL movesBackFirst = acc.z > 1.0;
        if ((isOnLeftSide && movesScreenFirst) || (isOnRightSide && movesBackFirst)) {
            motionMessage = @"hit:r";
        }
        else if ((isOnLeftSide && movesBackFirst) || (isOnRightSide && movesScreenFirst)) {
            motionMessage = @"hit:l";
        }
        if (motionMessage) {
            if (isUpright) {
                motionMessage = [NSString stringWithFormat:@"%@c", motionMessage];
            }
            [[app network] send:motionMessage];
        }
    }
    else if (ABS(acc.z) < 0.2) {
        motionMessage = nil;
    }
}

- (void)handleMessage:(NSString *)message {
    NSArray *tokens = [message componentsSeparatedByString: @":"];
    NSString *action = tokens[0];
    if ([action isEqualToString:@"lives"]) {
        youScore.text = tokens[1];
        oppScore.text = tokens[1];
    }
    else if ([action isEqualToString:@"serve"]) {
        [UIView animateWithDuration:0.5 animations:^{[waitView setAlpha:0.0];}];
        [[app sound] loop:@"service"];
    }
    else if ([action isEqualToString:@"recv"]) {
        [UIView animateWithDuration:0.5 animations:^{[waitView setAlpha:0.0];}];
    }
    else if ([action isEqualToString:@"good"]) {
        [[app sound] play:@"good"];
        youScore.text = tokens[1];
        oppScore.text = tokens[2];
    }
    else if ([action isEqualToString:@"fail"]) {
        [[app sound] play:@"fail"];
        youScore.text = tokens[1];
        oppScore.text = tokens[2];
    }
    else if ([action isEqualToString:@"win"]) {
        [[app sound] play:@"you_win"];
    }
    else if ([action isEqualToString:@"lose"]) {
        [[app sound] play:@"you_lose"];
    }
    else if ([action isEqualToString:@"ownb"]) {
        [self pan:@"bounce_close" withString:tokens[1]];
        [[app sound] play:@"bounce_close"];
    }
    else if ([action isEqualToString:@"oppb"]) {
        [[app sound] play:@"bounce_far"];
    }
    else if ([action isEqualToString:@"ownh"]) {
        [[app sound] stop:@"service"];
        [[app sound] play:@"hit_close"];
    }
    else if ([action isEqualToString:@"opph"]) {
        [self pan:@"hit_far" withString:tokens[1]];
        [[app sound] play:@"hit_far"];
    }
    else if ([action isEqualToString:@"miss"]) {
        [[app sound] play:@"swosh"];
    }
}

- (void)handleStatus:(NSString *)status onStream:(NSString *)ioStream {
    
}

- (IBAction)leaveMatch:(id)sender {
    [[app network] send:@"part"];
}

- (void)pan:(NSString *)sampleName withString:(NSString *)leftOrRight {
    float panValue;
    if ([leftOrRight isEqualToString:@"l"]) panValue = -0.9;
    else panValue = 0.9;
    [[app sound] pan:sampleName withValue:panValue];
}

@end
