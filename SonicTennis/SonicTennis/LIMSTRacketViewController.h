//
//  LIMSTRacketViewController.h
//  SonicTennis
//
//  Created by AAU-CPH0065 on 04/03/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LIMSTRacketViewController : UIViewController

@property(unsafe_unretained, nonatomic) IBOutlet UIImageView *racketImg;

@end
