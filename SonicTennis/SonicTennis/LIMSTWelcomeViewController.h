//
//  LIMSTWelcomeViewController.h
//  SonicTennis
//
//  Created by AAU-CPH0065 on 01/03/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LIMSTWelcomeViewController : UIViewController

@property (unsafe_unretained, nonatomic) IBOutlet UIView *buttonContainer;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *playButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *howtoButton;

- (IBAction)play:(id)sender;
- (IBAction)howto:(id)sender;

@end
