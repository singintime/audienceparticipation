//
//  LIMSTMatchesViewController.h
//  SonicTennis
//
//  Created by AAU-CPH0065 on 28/02/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LIMSTMatchViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, APNetworkDelegate>

@property (strong, nonatomic) NSMutableArray *matches;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *racketImg;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *matchName;
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *matchSelector;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *makeButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *joinButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *backButton;

- (IBAction)newMatch:(id)sender;
- (IBAction)joinMatch:(id)sender;
- (IBAction)back:(id)sender;

@end
