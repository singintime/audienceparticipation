//
//  LIMSTAppDelegate.h
//  SonicTennis
//
//  Created by AAU-CPH0065 on 10/01/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LIMSTHowtoViewController;
@class LIMSTMatchViewController;
@class LIMSTPlayViewController;
@class LIMSTRacketViewController;
@class LIMSTWelcomeViewController;

@interface LIMSTAppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate, APNetworkDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIAlertView *connectionAlert;
@property (strong, nonatomic) LIMSTHowtoViewController *howtoController;
@property (strong, nonatomic) LIMSTMatchViewController *matchController;
@property (strong, nonatomic) LIMSTPlayViewController *playController;
@property (strong, nonatomic) LIMSTRacketViewController *racketController;
@property (strong, nonatomic) LIMSTWelcomeViewController *welcomeController;
@property (strong, nonatomic) APNetworkManager *network;
@property (strong, nonatomic) APSoundManager *sound;

- (void)fadeInView:(UIView *)view;
- (void)fadeOutView:(UIView *)view;
- (void)crossFadeView:(UIView *)view;
- (void)sendPlay;

@end
