//
//  LIMSTHowtoViewController.h
//  SonicTennis
//
//  Created by AAU-CPH0065 on 01/03/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LIMSTHowtoViewController : UIViewController;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *howtoLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *tapLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *howtoImage;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *leftImage;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *rightImage;

- (IBAction)proceed:(id)sender;

@end
