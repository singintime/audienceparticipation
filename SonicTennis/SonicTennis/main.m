//
//  main.m
//  SonicTennis
//
//  Created by AAU-CPH0065 on 10/01/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LIMSTAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LIMSTAppDelegate class]));
    }
}
