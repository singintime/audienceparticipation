//
//  APConnectionViewController.m
//  APTennis
//
//  Created by AAU-CPH0065 on 04/12/2012.
//  Copyright (c) 2012 Medialogy Lab - Aalborg University Copenhagen. All rights reserved.
//

#import "APConnectionViewController.h"
#import "APAppDelegate.h"

@interface APConnectionViewController () {
    @private
    __unsafe_unretained APAppDelegate *app;
}
@end

@implementation APConnectionViewController
@synthesize connectImg, hostField, portField, msgLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        app = (APAppDelegate *)[[UIApplication sharedApplication] delegate];
        UITabBarItem *tbi = [self tabBarItem];
        UIImage *tbimg = [UIImage imageNamed:@"connect_icon.png"];
        [tbi setTitle:@"Connect"];
        [tbi setImage:tbimg];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIScreen *screen = [UIScreen mainScreen];
    CGSize size = screen.bounds.size;
    CGFloat scale = screen.scale;
    if (size.width == 320.0f && size.height == 568.0f && scale == 2.0f) {
        [connectImg setImage:[UIImage imageNamed:@"Default-568h.png"]];
    }
    [[app network] addObserver:self];
    [hostField setDelegate:self];
    [portField setDelegate:self];
}

- (void)viewDidUnload
{
    [[app network] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)connect:(id)sender
{
    [self textFieldShouldReturn:hostField];
    [self textFieldShouldReturn:portField];
    [msgLabel setText:@"Connecting..."];
    NSString *address = [[self hostField] text];
    NSInteger port = [[[self portField] text] integerValue];
    [[app network] connectWithAddress:address port:port];
}

- (void)handleMessage:(NSString *)message
{
}

- (void)handleStatus:(NSString *)status onStream:(NSString *)ioStream {
    if ([status isEqualToString:@"space"]) {
        [msgLabel setText:@"Succesfully connected!\nPlease wait for a game to start..."];
    }
    else if ([status isEqualToString:@"closed"] || [status isEqualToString:@"error"]) {
        [msgLabel setText:@"Connection error!\nPlease try to reconnect..."];
    }
    else {
        [msgLabel setText:status];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end