//
//  APTennisAppDelegate.h
//  APTennis
//
//  Created by AAU-CPH0065 on 27/11/2012.
//  Copyright (c) 2012 Medialogy Lab - Aalborg University Copenhagen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APFramework.h"
#import "PdAudioController.h"
#import "APConnectionViewController.h"

@interface APAppDelegate : UIResponder <UIApplicationDelegate, APNetworkDelegate>

@property (strong, nonatomic) PdAudioController *pdAudio;
@property (strong, nonatomic) APMotionManager *motion;
@property (strong, nonatomic) APNetworkManager *network;
@property (strong, nonatomic) APConnectionViewController *connView;
@property (strong, nonatomic) UIWindow *window;

@end
