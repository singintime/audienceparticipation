//
//  APPinhataViewController.m
//  APTennis
//
//  Created by AAU-CPH0065 on 13/12/2012.
//  Copyright (c) 2012 Medialogy Lab - Aalborg University Copenhagen. All rights reserved.
//

#import "PdBase.h"
#import "APPinhataViewController.h"
#import "APAppDelegate.h"

@interface APPinhataViewController () {
    @private
    __unsafe_unretained APAppDelegate *app;
    CGFloat accT, rotT, attT;
    AVCaptureSession *session;
    NSString *gesture;
}

@end

@implementation APPinhataViewController
@synthesize camView, staffImg;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        app = (APAppDelegate *)[[UIApplication sharedApplication] delegate];
        accT = 1.0;
        rotT = 1.0;
        attT = 0.7;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    session = [[AVCaptureSession alloc] init];
	[session setSessionPreset:AVCaptureSessionPresetHigh];
	AVCaptureVideoPreviewLayer *captureLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    [captureLayer setFrame:[camView bounds]];
    [[camView layer] addSublayer:captureLayer];
	AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
	AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
	if (!input) {
		// Handle the error appropriately.
		NSLog(@"ERROR: trying to open camera: %@", error);
	}
	[session addInput:input];
	[session startRunning];
    gesture = nil;
    [[app motion] addObserver:self];
    [[app network] addObserver:self];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [session stopRunning];
    session = nil;
    gesture = nil;
    [[app motion] removeObserver:self];
    [[app network] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIScreen *screen = [UIScreen mainScreen];
    CGSize size = screen.bounds.size;
    CGFloat scale = screen.scale;
    if (size.width == 320.0f && size.height == 568.0f && scale == 2.0f) {
        [staffImg setImage:[UIImage imageNamed:@"staff-568h.png"]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleMotionWithAcceleration:(CMAcceleration)acc rotation:(CMRotationRate)rot attitude:(CMAttitude *)att {
    [PdBase sendFloat:(acc.x + acc.y + acc.z) toReceiver: @"speed"];
    if (!gesture) {
        if (att.pitch > M_PI_2 - attT && acc.z > accT) {
            gesture = @"hit";
            [[app network] send:gesture];
        }
    }
    else if (ABS(acc.z) < 0.2 * accT) {
        gesture = nil;
        return;
    }
}

- (void)handleMessage:(NSString *)message {
    NSArray *tokens = [message componentsSeparatedByString: @":"];
    NSString *action = tokens[0];
    if ([action isEqualToString:@"thud"]) {
        [PdBase sendBangToReceiver:@"thud"];
    }
    else if ([action isEqualToString:@"crash"]) {
        [PdBase sendBangToReceiver:@"crash"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Pinhata crashed!"
                                                        message:[NSString stringWithFormat:@"You hit %@ times", tokens[1]]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if ([action isEqualToString:@"win"]) {
        [PdBase sendBangToReceiver:@"good"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You won!"
                                                        message:@"Congratulations, you won!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)handleStatus:(NSString *)status onStream:(NSString *)ioStream {
    
}

@end
