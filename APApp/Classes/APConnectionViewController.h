//
//  APConnectionViewController.h
//  APTennis
//
//  Created by AAU-CPH0065 on 04/12/2012.
//  Copyright (c) 2012 Medialogy Lab - Aalborg University Copenhagen. All rights reserved.
//

#import "APFramework.h"
#import <UIKit/UIKit.h>

@interface APConnectionViewController : UIViewController <UITextFieldDelegate, APNetworkDelegate>

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *connectImg;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *hostField, *portField;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *msgLabel;

- (IBAction)connect:(id)sender;
- (BOOL)textFieldShouldReturn:(UITextField *)textField;

@end
