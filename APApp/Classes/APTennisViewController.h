//
//  APTennisViewController.h
//  APTennis
//
//  Created by AAU-CPH0065 on 27/11/2012.
//  Copyright (c) 2012 Medialogy Lab - Aalborg University Copenhagen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APFramework.h"

@interface APTennisViewController : UIViewController <APMotionDelegate, APNetworkDelegate>

@property (strong, nonatomic) NSMutableArray *teams;

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *racketImg;
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *teamSelector;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *partButton;

- (IBAction)part:(id)sender;

@end
