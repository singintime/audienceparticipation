//
//  APTennisViewController.m
//  APTennis
//
//  Created by AAU-CPH0065 on 27/11/2012.
//  Copyright (c) 2012 Medialogy Lab - Aalborg University Copenhagen. All rights reserved.
//

#import "APTennisViewController.h"
#import "APAppDelegate.h"
#import "PdBase.h"

@interface APTennisViewController () {
    @private
    __unsafe_unretained APAppDelegate *app;
    CGFloat accT, rotT, attT;
    NSInteger hitT;
    NSString *gesture;
}
@end

@implementation APTennisViewController
@synthesize teams, racketImg, teamSelector, partButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        app = (APAppDelegate *)[[UIApplication sharedApplication] delegate];
        accT = 0.5;
        rotT = 1.0;
        attT = 0.7;
        hitT = 500;
        teams = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    gesture = nil;
    [[app motion] addObserver:self];
    [[app network] addObserver:self];
    [[app network] send:@"list"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    gesture = nil;
    [[app motion] removeObserver:self];
    [[app network] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    UIScreen *screen = [UIScreen mainScreen];
    CGSize size = screen.bounds.size;
    CGFloat scale = screen.scale;
    if (size.width == 320.0f && size.height == 568.0f && scale == 2.0f) {
        [racketImg setImage:[UIImage imageNamed:@"racket-568h.png"]];
    }
    [teamSelector setHidden:NO];
    [partButton setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Select a team:";
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [teams count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Identifier for retrieving reusable cells.
    static NSString *cellIdentifier = @"TeamsCellIdentifier";
    
    // Attempt to request the reusable cell.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // No cell available - create one.
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    
    // Set the text of the cell to the row index.
    NSString *team_id = [teams objectAtIndex:[indexPath row]];
    NSArray *tokens = [team_id componentsSeparatedByString: @"_"];
    cell.textLabel.text = tokens[0];
    cell.detailTextLabel.text = team_id;
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[app network] send:[NSString stringWithFormat:@"join:%@", [teams objectAtIndex:[indexPath row]]]];
}

- (IBAction)part:(id)sender {
    [[app network] send:@"part"];
}

- (void)handleMotionWithAcceleration:(CMAcceleration)acc rotation:(CMRotationRate)rot attitude:(CMAttitude *)att {
    [PdBase sendFloat:(acc.x + acc.y + acc.z) toReceiver: @"speed"];
    if (!gesture) {
        if (ABS(att.pitch) < attT) {
            if (att.roll > M_PI_2 - attT && att.roll < M_PI_2 + attT) {
                if (acc.z < - accT && rot.x > rotT) {
                    gesture = @"hit:l";
                }
                else if (acc.z > accT && rot.x < -rotT) {
                    gesture = @"hit:r";
                }
            }
            else if (-att.roll > M_PI_2 - attT && -att.roll < M_PI_2 + attT) {
                if (acc.z < - accT && rot.x > rotT) {
                    gesture = @"hit:r";
                }
                else if (acc.z > accT && rot.x < -rotT) {
                    gesture = @"hit:l";
                }
            }
        }
        else if (att.pitch > M_PI_2 - attT && acc.z < - accT && rot.x > rotT) {
            gesture = @"hit:c";
        }
        if (gesture) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[app network] send:[NSString stringWithFormat:@"%@", gesture]];
            });
        }
    }
    else if (ABS(acc.z) < 0.66 * accT) {
        gesture = nil;
    }
}

- (void)handleMessage:(NSString *)message {
    NSArray *tokens = [message componentsSeparatedByString: @":"];
    NSString *action = tokens[0];
    if ([action isEqualToString:@"good"]) {
        [PdBase sendBangToReceiver:@"hit"];
        [PdBase sendBangToReceiver:@"good"];
    }
    else if ([action isEqualToString:@"fail"]) {
        NSInteger lives = [tokens[1] integerValue];
        if (lives > 0) {
            [PdBase sendBangToReceiver:@"fail"];
        }
        else {
            [PdBase sendBangToReceiver:@"over"];
        }
    }
    else if ([action isEqualToString:@"new"]) {
        [teams removeObject:tokens[1]];
        [teams addObject:tokens[1]];
        [teamSelector reloadData];
    }
    else if ([action isEqualToString:@"del"]) {
        [teams removeObject:tokens[1]];
        [teamSelector reloadData];
    }
    else if ([action isEqualToString:@"join"]) {
        [teamSelector setHidden:YES];
        [partButton setHidden:NO];
    }
    else if ([action isEqualToString:@"part"]) {
        [teamSelector setHidden:NO];
        [partButton setHidden:YES];
    }
}

- (void)handleStatus:(NSString *)status onStream:(NSString *)ioStream {
}

@end