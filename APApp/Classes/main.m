//
//  main.m
//  APTennis
//
//  Created by AAU-CPH0065 on 27/11/2012.
//  Copyright (c) 2012 Medialogy Lab - Aalborg University Copenhagen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "APAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([APAppDelegate class]));
    }
}
