//
//  APPinhataViewController.h
//  APTennis
//
//  Created by AAU-CPH0065 on 13/12/2012.
//  Copyright (c) 2012 Medialogy Lab - Aalborg University Copenhagen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "APFramework.h"

@interface APPinhataViewController : UIViewController <APMotionDelegate, APNetworkDelegate>

@property (unsafe_unretained, nonatomic) IBOutlet UIView *camView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *staffImg;

@end
