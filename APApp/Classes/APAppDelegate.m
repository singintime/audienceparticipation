//
//  APTennisAppDelegate.m
//  APTennis
//
//  Created by AAU-CPH0065 on 27/11/2012.
//  Copyright (c) 2012 Medialogy Lab - Aalborg University Copenhagen. All rights reserved.
//

#import "APAppDelegate.h"
#import "APTennisViewController.h"
#import "APPinhataViewController.h"
#import "PdBase.h"

@interface APAppDelegate() {
    @private
    BOOL isRegistered;
    APTennisViewController *tennisView;
    APPinhataViewController *pinhataView;
}
@end

@implementation APAppDelegate
@synthesize pdAudio, motion, network, connView, window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    isRegistered = NO;
	pdAudio = [[PdAudioController alloc] init];
    [pdAudio configurePlaybackWithSampleRate:44100.0 numberChannels:2 inputEnabled:YES mixingEnabled:YES];
    [PdBase openFile:@"aptennis.pd" path:[[NSBundle mainBundle] bundlePath]];
	[pdAudio setActive:YES];
	[pdAudio print];
    motion = [[APMotionManager alloc] init];
    [motion start];
    network = [[APNetworkManager alloc] init];
    [network addObserver:self];
    connView = [[APConnectionViewController alloc] initWithNibName:@"APConnectionViewController" bundle:nil];
    tennisView = [[APTennisViewController alloc] initWithNibName:@"APTennisViewController" bundle:nil];
    pinhataView = [[APPinhataViewController alloc] initWithNibName:@"APPinhataViewController" bundle:nil];
    [window setRootViewController:connView];
    [window makeKeyAndVisible];
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)handleMessage:(NSString *)message {
    NSLog(@"%@", message);
    NSArray *tokens = [message componentsSeparatedByString: @":"];
    NSString *action = tokens[0];
    if ([action isEqualToString:@"play"]) {
        if ([tokens[1] isEqualToString:@"APHall"] && [tokens[2] isEqualToString:@"1"]) {
            [network send:@"getgame"];
        }
        else if ([tokens[1] isEqualToString:@"APTennis"] && [tokens[2] isEqualToString:@"1"]) {
            [window setRootViewController:tennisView];
        }
        else if ([tokens[1] isEqualToString:@"APPinhata"] && [tokens[2] isEqualToString:@"1"]) {
            [window setRootViewController:pinhataView];
        }
    }
    if ([action isEqualToString:@"change"]) {
        [network send:[NSString stringWithFormat:@"play:%@:%@", tokens[1], tokens[2]]];
    }
}

- (void)handleStatus:(NSString *)status onStream:(NSString *)ioStream {
    if ([status isEqualToString:@"space"] && [ioStream isEqualToString:@"out"]) {
        if (!isRegistered) {
            [network send:@"play:APHall:1"];
            isRegistered = YES;
        }
    }
    else if ([status isEqualToString:@"closed"] || [status isEqualToString:@"error"]) {
        [window setRootViewController:connView];
        isRegistered = NO;
    }
}

@end
