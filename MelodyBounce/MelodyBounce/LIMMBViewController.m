//
//  LIMSTPlayViewController.m
//  SonicTennis
//
//  Created by AAU-CPH0065 on 14/01/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import "LIMMBViewController.h"
#import "LIMMBAppDelegate.h"

@interface LIMMBViewController () {
    NSString *selectedMatch;
    LIMMBAppDelegate *app;
}

@end

@implementation LIMMBViewController
@synthesize matches, racketImg, settingsContainer, matchName, matchSelector;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        matches = [[NSMutableArray alloc] init];
        app = (LIMMBAppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [settingsContainer setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
    UIScreen *screen = [UIScreen mainScreen];
    CGSize size = screen.bounds.size;
    CGFloat scale = screen.scale;
    if (size.width == 320.0f && size.height == 568.0f && scale == 2.0f) {
        [racketImg setImage:[UIImage imageNamed:@"notes-568h.png"]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [matches count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Identifier for retrieving reusable cells.
    static NSString *cellIdentifier = @"MatchesCellIdentifier";
    
    // Attempt to request the reusable cell.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // No cell available - create one.
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    
    // Set the text of the cell to the row index.
    NSString *match_id = [matches objectAtIndex:[indexPath row]];
    NSArray *tokens = [match_id componentsSeparatedByString: @"_"];
    cell.textLabel.text = tokens[0];
    cell.detailTextLabel.text = match_id;
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedMatch = [matches objectAtIndex:[indexPath row]];
}

- (void)addMatch:(NSString *)match {
    [matches removeObject:match];
    [matches addObject:match];
    for (NSString *s in matches) {
        NSLog(@"%@", s);
    }
    [matchSelector reloadData];
    NSLog(@"%d", [self tableView:matchSelector numberOfRowsInSection:0]);
}

- (void)delMatch:(NSString *)match {
    [matches removeObject:match];
    [matchSelector reloadData];
}

- (IBAction)newMatch:(id)sender {
    if (![matchName.text isEqualToString:@""]) {
        [[app network] send:[NSString stringWithFormat:@"make:%@", matchName.text]];
    }
}

- (IBAction)joinMatch:(id)sender {
    if (selectedMatch) {
        [[app network] send:[NSString stringWithFormat:@"join:%@", selectedMatch]];
    }
}

- (IBAction)fakeShake:(id)sender {
    [[app network] send:@"shake"];
}
@end
