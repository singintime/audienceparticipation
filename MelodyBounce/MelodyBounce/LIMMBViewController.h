//
//  LIMSTPlayViewController.h
//  SonicTennis
//
//  Created by AAU-CPH0065 on 14/01/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LIMMBViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSMutableArray *matches;
@property (weak, nonatomic) IBOutlet UIImageView *racketImg;
@property (weak, nonatomic) IBOutlet UIView *settingsContainer;
@property (weak, nonatomic) IBOutlet UITextField *matchName;
@property (weak, nonatomic) IBOutlet UITableView *matchSelector;

- (void)addMatch:(NSString *)match;
- (void)delMatch:(NSString *)match;
- (IBAction)newMatch:(id)sender;
- (IBAction)joinMatch:(id)sender;
- (IBAction)fakeShake:(id)sender;
@end
