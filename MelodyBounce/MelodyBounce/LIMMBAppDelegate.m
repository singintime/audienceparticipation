//
//  LIMMBAppDelegate.m
//  MelodyBounce
//
//  Created by AAU-CPH0065 on 17/01/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import "LIMMBAppDelegate.h"

#import "LIMMBViewController.h"

@interface LIMMBAppDelegate () {
    @private
    NSString *gesture;
    CGFloat threshold;
    BOOL isRegistered;
}
@end

@implementation LIMMBAppDelegate

@synthesize viewController, motion, network, sound;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    motion = [[APMotionManager alloc] initWithUpdateInterval:0.04];
    network = [[APNetworkManager alloc] init];
    sound = [[APSoundManager alloc] init];
    [motion addObserver:self];
    [network addObserver:self];
    [sound addSamples:[NSArray arrayWithObjects:@"C", @"C#", @"D", @"D#", @"E", @"F", @"F#", @"G", @"G#", @"A", @"A#", @"B", @"HC", @"good", @"fail", nil] ofType:@"wav"];
    gesture = nil;
    threshold = 1.0;
    isRegistered = NO;
    [motion start];
    [network connectWithAddress:@"centos6.lim.di.unimi.it" port:27986];
    self.viewController = [[LIMMBViewController alloc] initWithNibName:@"LIMMBViewController" bundle:nil];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [motion removeObserver:self];
    [network removeObserver:self];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)handleMotionWithAcceleration:(CMAcceleration)acc rotation:(CMRotationRate)rot attitude:(CMAttitude *)att {
    if (!gesture && ABS(acc.x) + ABS(acc.y) + ABS(acc.z) > threshold) {
        gesture = @"shake";
        [network send:gesture];
    }
    if (gesture && ABS(acc.x) + ABS(acc.y) + ABS(acc.z) < 0.25 * threshold) {
        gesture = nil;
    }
}

- (void)handleMessage:(NSString *)message {
    NSArray *tokens = [message componentsSeparatedByString: @":"];
    NSString *action = [tokens objectAtIndex:0];
    if ([action isEqualToString:@"play"]) {
        [network send:@"list"];
    }
    else if ([action isEqualToString:@"new"]) {
        [viewController addMatch:tokens[1]];
    }
    else if ([action isEqualToString:@"del"]) {
        [viewController delMatch:tokens[1]];
    }
    else if ([action isEqualToString:@"join"]) {
        [[viewController settingsContainer] setHidden:YES];
    }
    else if ([action isEqualToString:@"note"]) {
        [sound play:tokens[1]];
    }
    else if ([action isEqualToString:@"win"]) {
        [sound play:@"good"];
    }
    else if ([action isEqualToString:@"fail"]) {
        [sound play:@"fail"];
    }
    else if ([action isEqualToString:@"part"]) {
        [[viewController settingsContainer] setHidden:NO];
    }
    else if ([action isEqualToString:@"nack"]) {
        NSLog(@"%@", message);
    }
}

- (void)handleStatus:(NSString *)status onStream:(NSString *)ioStream {
    NSLog(@"%@", status);
    if ([status isEqualToString:@"space"] && [ioStream isEqualToString:@"out"]) {
        if (!isRegistered) {
            [network send:@"play:MelodyBounce:1"];
            isRegistered = YES;
        }
    }
}

@end
