//
//  main.m
//  MelodyBounce
//
//  Created by AAU-CPH0065 on 17/01/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LIMMBAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LIMMBAppDelegate class]));
    }
}
