//
//  LIMMBAppDelegate.h
//  MelodyBounce
//
//  Created by AAU-CPH0065 on 17/01/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APFramework.h"

@class LIMMBViewController;

@interface LIMMBAppDelegate : UIResponder <UIApplicationDelegate, APMotionDelegate, APNetworkDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) LIMMBViewController *viewController;

@property (strong, nonatomic) APMotionManager *motion;
@property (strong, nonatomic) APNetworkManager *network;
@property (strong, nonatomic) APSoundManager *sound;

@end
