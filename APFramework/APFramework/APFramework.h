//
//  APFramework.h
//  APFramework
//
//  Created by AAU-CPH0065 on 14/01/2013.
//  Copyright (c) 2013 Laboratorio di Informatica Musicale - University of Milan. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <CoreMotion/CoreMotion.h>
#import <Foundation/Foundation.h>

@protocol APMotionDelegate <NSObject>
@required
- (void)handleMotionWithAcceleration:(CMAcceleration)acc rotation:(CMRotationRate)rot attitude:(CMAttitude *)att;
@end

@protocol APNetworkDelegate <NSObject>
@required
- (void)handleMessage:(NSString *)message;
- (void)handleStatus:(NSString *)status onStream:(NSString *)ioStream;
@end

@interface APMotionManager : NSObject
- (void)addObserver:(__unsafe_unretained id <APMotionDelegate>)observer;
- (id)initWithUpdateInterval:(NSTimeInterval)interval;
- (void)handleMotion:(CMDeviceMotion *)deviceMotion error:(NSError *)error;
- (void)removeObserver:(id <APMotionDelegate>)observer;
- (void)setUpdateInterval:(NSTimeInterval)updateInterval;
- (void)start;
- (void)stop;
@end

@interface APNetworkManager : NSObject <NSStreamDelegate>
- (void)addObserver:(__unsafe_unretained id <APNetworkDelegate>)observer;
- (void)connectWithAddress:(NSString *)address port:(NSInteger)port;
- (void)closeConnection;
- (void)removeObserver:(id <APNetworkDelegate>)observer;
- (NSInteger)send:(NSString *)message;
@end

@interface APSoundManager : NSObject
- (void)addSampleNamed:(NSString *)sampleName ofType:(NSString *)type;
- (void)addSamples:(NSArray *)sampleNames ofType:(NSString *)type;
- (void)clear;
- (void)pan:(NSString *)soundName withValue:(float)value;
- (void)loop:(NSString *)soundName;
- (void)play:(NSString *)soundName;
- (void)play:(NSString *)soundName withLoops:(NSInteger)loops;
- (void)removeSampleNamed:(NSString *)sampleName;
- (void)stop:(NSString *)sampleName;
@end