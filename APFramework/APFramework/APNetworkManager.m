//
//  APTennisNetworkController.m
//  APTennis
//
//  Created by AAU-CPH0065 on 01/12/2012.
//  Copyright (c) 2012 Medialogy Lab - Aalborg University Copenhagen. All rights reserved.
//

#import "APFramework.h"

@interface APNetworkManager () {
    @private
    NSInputStream *in;
    NSOutputStream *out;
    NSMutableSet *observers;
}
@end

@implementation APNetworkManager
- (id)init {
    self = [super init];
    if (self) {
        observers = [[NSMutableSet alloc] init];
    }
    return self;
}

- (void)addObserver:(__unsafe_unretained id <APNetworkDelegate>)observer {
    @synchronized(observers) {
        [observers addObject:observer];
    }
}

- (void)connectWithAddress:(NSString *)address port:(NSInteger)port {
    NSLog(@"Attempting connection");
    if (in) [in close];
    if (out) [out close];
    in = nil;
    out = nil;
    CFReadStreamRef read;
    CFWriteStreamRef write;
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)address, port, &read, &write);
    in = (__bridge NSInputStream *)read;
    out = (__bridge NSOutputStream *)write;
    [in setDelegate:self];
    [out setDelegate:self];
    [in scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [out scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [in open];
    [out open];
}

- (void)closeConnection {
    if (in) [in close];
    if (out) [out close];
    in = nil;
    out = nil;
}

- (void)handleMessage:(NSString *)message {
    for (__unsafe_unretained id <APNetworkDelegate> observer in observers) {
        @try {
            [observer handleMessage:message];
        }
        @catch (NSException *e) {}
    }
}

- (void)handleStatus:(NSString *)status onStream:(NSString *)stream {
    NSSet *observersCopy = [NSSet setWithSet:observers];
    for (id <APNetworkDelegate> observer in observersCopy) {
        @try {
            [observer handleStatus:status onStream:stream];
        }
        @catch (NSException *e) {}
    }
}

- (void)removeObserver:(id <APNetworkDelegate>)observer {
    [observers removeObject:observer];
}

- (NSInteger)send:(NSString *)msg {
    NSString *output = [NSString stringWithFormat:@"%@\n", msg];
    NSInteger result = 0;
    if ([out hasSpaceAvailable]) {
        NSData *data = [[NSData alloc] initWithData:[output dataUsingEncoding:NSASCIIStringEncoding]];
        result = [out write:[data bytes] maxLength:[data length]];
    }
    if (result == 0) {
        for (id <APNetworkDelegate> observer in observers) {
            [self handleStatus:@"sendfailed" onStream:@"out"];
        }
        
    }
    return result;
}

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode {
    NSString *ioString;
    ioString = aStream == in ? @"in" : @"out";
    switch (eventCode) {
        case NSStreamEventEndEncountered:
            [self handleStatus:@"closed" onStream:ioString];
            break;
        case NSStreamEventErrorOccurred:
            [self handleStatus:@"closed" onStream:ioString];
            break;
        case NSStreamEventHasBytesAvailable:
            if (aStream == in) {
                while ([in hasBytesAvailable]) {
                    uint8_t buffer[256];
                    int len = [in read:buffer maxLength:sizeof(buffer)];
                    if (len > 0) {
                        NSString *input = [[NSString alloc] initWithBytes:buffer
                                                                   length:len
                                                                 encoding:NSASCIIStringEncoding];
                        NSArray *lines = [input componentsSeparatedByString: @"\n"];
                        for (NSString *line in lines) {
                            [self handleMessage:line];
                        }
                    }
                }
            }
            break;
        case NSStreamEventHasSpaceAvailable:
        {
            [self handleStatus:@"space" onStream:ioString];
            break;
        }
        case NSStreamEventNone:
            [self handleStatus:@"error" onStream:ioString];
            break;
        case NSStreamEventOpenCompleted:
            [self handleStatus:@"ok" onStream:ioString];
            break;
        default:
            break;
    }
}

@end
