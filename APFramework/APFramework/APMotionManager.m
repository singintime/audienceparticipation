//
//  APTennisMotionController.m
//  APTennis
//
//  Created by AAU-CPH0065 on 01/12/2012.
//  Copyright (c) 2012 Medialogy Lab - Aalborg University Copenhagen. All rights reserved.
//

#import "APFramework.h"

@interface APMotionManager () {
    @private
    CMMotionManager *motion;
    NSOperationQueue *motionQueue;
    NSMutableSet *observers;
}

@end

@implementation APMotionManager
- (id)init {
    return [self initWithUpdateInterval:0.04];
}

- (id)initWithUpdateInterval:(NSTimeInterval)interval {
    self = [super init];
    if (self) {
        observers = [[NSMutableSet alloc] init];
        motion = [[CMMotionManager alloc] init];
        motionQueue = [[NSOperationQueue alloc] init];
        [motion setDeviceMotionUpdateInterval:interval];
    }
    return self;
}

- (void)addObserver:(__unsafe_unretained id <APMotionDelegate>)observer {
        [observers addObject:observer];
}

- (void)handleMotion:(CMDeviceMotion *)deviceMotion error:(NSError *)error {
    CMAcceleration acc = [deviceMotion userAcceleration];
    CMRotationRate rot = [deviceMotion rotationRate];
    CMAttitude *att = [deviceMotion attitude];
    NSSet *observersCopy = [NSSet setWithSet:observers];
    for (__unsafe_unretained id <APMotionDelegate> observer in observersCopy) {
        @try {
            [observer handleMotionWithAcceleration:acc rotation:rot attitude:att];
        }
        @catch (NSException *e) {}
    }
}

- (void)removeObserver:(id <APMotionDelegate>)observer {
    [observers removeObject:observer];
}

- (void)setUpdateInterval:(NSTimeInterval)updateInterval {
    [motion setDeviceMotionUpdateInterval:updateInterval];
}

- (void)start {
    [motion startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXArbitraryZVertical
                                                toQueue:motionQueue
                                            withHandler:^(CMDeviceMotion *dm, NSError *e) {
                                                [self handleMotion:dm error:e];
                                            }];
}

- (void)stop {
    [motion stopDeviceMotionUpdates];
}

@end
