//
//  APSoundManager.m
//  APTennis
//
//  Created by AAU-CPH0065 on 13/12/2012.
//  Copyright (c) 2012 Medialogy Lab - Aalborg University Copenhagen. All rights reserved.
//

#import "APFramework.h"

@interface APSoundManager () {
    @private
    NSMutableDictionary *samples;
}
@end

@implementation APSoundManager
- (id)init {
    self = [super init];
    if (self) {
        samples = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)addSampleNamed:(NSString *)sampleName ofType:(NSString *)type {
    NSString *path = [[NSBundle mainBundle] pathForResource:sampleName ofType:type];
    if (path) {
        NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
        AVAudioPlayer *sample = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
        [samples setValue:sample forKey:sampleName];
    }
}

- (void)addSamples:(NSArray *)sampleNames ofType:(NSString *)type {
    for (NSString *sampleName in sampleNames) {
        [self addSampleNamed:sampleName ofType:type];
    }
}

- (void)clear {
    [samples removeAllObjects];
}

- (void)pan:(NSString *)sampleName withValue:(float)value {
    AVAudioPlayer *sound = [samples objectForKey:sampleName];
    if (sound) {
        [sound setPan:value];
    }
}

- (void)loop:(NSString *)sampleName {
    [self play:sampleName withLoops:-1];
}

- (void)play:(NSString *)sampleName {
    [self play:sampleName withLoops:0];
}

- (void)play:(NSString *)sampleName withLoops:(NSInteger)loops {
    AVAudioPlayer *sound = [samples objectForKey:sampleName];
    [sound setNumberOfLoops:loops];
    if (sound) {
        [sound stop];
        [sound setCurrentTime:0];
        [sound play];
    }
}

- (void)removeSampleNamed:(NSString *)sampleName {
    [samples removeObjectForKey:sampleName];
}

- (void)stop:(NSString *)sampleName {
    AVAudioPlayer *sound = [samples objectForKey:sampleName];
    if (sound) {
        [sound stop];
    }
}

@end
